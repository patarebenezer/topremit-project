import React, { useMemo, useRef, useState } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import BottomSheet from "@gorhom/bottom-sheet";
import Ionicons from "@expo/vector-icons/Ionicons";
import Card from "./src/components/Card";
import Survey from "./src/components/Survey";
import Rating from "./src/components/Rating";
import CardText from "./src/components/CardText";
import appStyles from "./src/styles/appStyles";

export default function App() {
 const bottomSheetRef = useRef(null);
 const [rating, setRating] = useState(0);
 const [openUp, setOpenUp] = useState(false);
 const snapPoints = useMemo(() => ["5%", "50%", "70%", "95%"], []);

 const openBottomSheet = () => {
  bottomSheetRef.current?.expand();
  setOpenUp(true);
 };
 const closeBottomSheet = () => {
  setRating(0);
  setOpenUp(false);
  bottomSheetRef.current?.close();
 };

 const handleRatingChange = (newRating) => {
  setRating(newRating);
 };

 return (
  <GestureHandlerRootView style={appStyles.container}>
   <View style={appStyles.content} onTouchEnd={() => openBottomSheet()}>
    <View style={appStyles.border}>
     <CardText openUp={openUp} />
     <Rating handleRatingChange={handleRatingChange} rating={rating} />
    </View>
   </View>
   <BottomSheet
    index={openUp && rating === 0 ? 1 : openUp && rating > 0 ? 3 : 0}
    snapPoints={snapPoints}
    ref={bottomSheetRef}
   >
    <View style={appStyles.bottomSheetContent}>
     <View
      style={appStyles.containerIcon}
      onTouchEnd={() => closeBottomSheet()}
     >
      <Ionicons name='close-outline' size={32} color='gray' />
     </View>
     <Text style={appStyles.textSuccess}>Transaction Success</Text>
     <Card />
     <Rating handleRatingChange={handleRatingChange} rating={rating} />
     <Survey rating={rating} />
    </View>
    <TouchableOpacity
     style={appStyles.btnSubmit}
     onPress={() => closeBottomSheet()}
    >
     <Text style={appStyles.textBtn}>Submit</Text>
    </TouchableOpacity>
   </BottomSheet>
  </GestureHandlerRootView>
 );
}

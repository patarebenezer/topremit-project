import { Text, View, Image } from "react-native";
import { AirbnbRating as RatingTopRemit } from "react-native-ratings";
import ratingStyles from "../styles/ratingStyles";

const Rating = ({ rating, handleRatingChange }) => {
 const coolImage = require("../assets/cool.png");
 const sucksImage = require("../assets/sucks.png");

 return (
  <View style={ratingStyles.container}>
   <Text style={ratingStyles.fontBold}>How's your transaction experience?</Text>
   {rating > 0 &&
    (rating > 3 ? <Image source={coolImage} /> : <Image source={sucksImage} />)}

   <RatingTopRemit
    defaultRating={rating}
    onFinishRating={handleRatingChange}
    showRating={false}
   />
  </View>
 );
};

export default Rating;

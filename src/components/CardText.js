import { Text, View, Image } from "react-native";
import cardStyles from "../styles/cardStyles";

const CardText = ({ openUp }) => {
 const flag = require("../assets/flag.png");

 return (
  <View style={{ display: openUp ? "none" : "block" }}>
   <View style={cardStyles.containerCards}>
    <Text style={cardStyles.textSuccess}>Transaction Success</Text>
    <Text style={cardStyles.textCardDates}>23 June</Text>
   </View>
   <View style={cardStyles.containerCards}>
    <View style={cardStyles.cardView}>
     <Image source={flag} style={cardStyles.imageView} />
     <Text>Patar Siahaan</Text>
    </View>
    <Text style={cardStyles.textMoneys}>EUR 5,000 </Text>
   </View>
  </View>
 );
};

export default CardText;

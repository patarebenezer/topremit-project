import React from "react";
import { Text, View, Image } from "react-native";
import cardStyles from "../styles/cardStyles";

const Card = () => {
 const flag = require("../assets/flag.png");

 return (
  <View style={cardStyles.container}>
   <View style={cardStyles.containerCard}>
    <Image source={flag} style={{ width: 37, height: 25, margin: 35 }} />
    <View style={cardStyles.containerName}>
     <Text style={cardStyles.textCard}>Patar Siahaan</Text>
     <Text style={cardStyles.textMoney}>5.000 EUR</Text>
    </View>
   </View>
   <Text style={cardStyles.textCardDate}>23 June</Text>
  </View>
 );
};

export default Card;

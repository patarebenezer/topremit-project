import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import styles from "../styles/surveyStyles";

const Survey = ({ rating }) => {
 const [selectedOptions, setSelectedOptions] = useState([]);

 const handleOptionPress = (index) => {
  const selectedIndex = selectedOptions.indexOf(index);
  if (selectedIndex === -1) {
   setSelectedOptions([...selectedOptions, index]);
  } else {
   const newSelectedOptions = [...selectedOptions];
   newSelectedOptions.splice(selectedIndex, 1);
   setSelectedOptions(newSelectedOptions);
  }
 };

 const renderOptions = () => {
  const optionsCool = [
   { text: "xxxx xxxxxxx xxxxx" },
   { text: "xxxxxxxxxx xxxxx" },
   { text: "xxxx xx xxx" },
   { text: "xxxx xxxxxxxxxxx" },
   { text: "xxxxxxxx xxxxxxxx xxxxxxxx" },
  ];

  const optionsSuck = [
   { text: "zzzz zzzzzzz zzzzz" },
   { text: "zzzzzzz zzzzz" },
   { text: "zzzz zz zzz" },
   { text: "zzzz zzzzzzzzzz zzz" },
   { text: "zzzzzzz zzzzzzzz" },
  ];

  if (rating === 0) {
   return null;
  }

  const options = rating > 3 ? optionsCool : optionsSuck;

  return options.map((option, index) => (
   <TouchableOpacity
    key={index}
    style={[
     styles.option,
     selectedOptions.includes(index) ? styles.optionSelected : null,
     index % 2 === 1 ? styles.optionRight : null,
    ]}
    onPress={() => handleOptionPress(index)}
   >
    <Text style={styles.textBtn}>{option.text}</Text>
   </TouchableOpacity>
  ));
 };

 return (
  <>
   <View style={{ display: rating === 0 ? "none" : "block" }}>
    <Text style={styles.marginTop20}>Tell us what makes you satifised</Text>
    <Text style={styles.textError}>*Required</Text>
   </View>
   <View style={styles.container}>{renderOptions()}</View>
  </>
 );
};

export default Survey;

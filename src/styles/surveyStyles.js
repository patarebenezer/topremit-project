import { StyleSheet } from "react-native";

const surveyStyles = StyleSheet.create({
 container: {
  flexDirection: "row",
  flexWrap: "wrap",
  marginBottom: 30,
  marginTop: 30,
 },
 option: {
  flexBasis: "48%",
  padding: 5,
  borderRadius: 80,
  borderColor: "#9BB0C1",
  borderWidth: 1.5,
  marginBottom: 10,
 },
 optionRight: {
  marginLeft: "4%",
 },
 optionSelected: {
  borderColor: "#50C4ED",
 },
 textBtn: {
  fontSize: 16,
  textAlign: "center",
  fontWeight: "500",
 },
 textError: {
  color: "red",
  marginTop: 10,
 },
 marginTop20: {
  marginTop: 20,
 },
});

export default surveyStyles;

import { StyleSheet } from "react-native";

const appStyles = StyleSheet.create({
 container: {
  flex: 1,
 },
 content: {
  flex: 1,
  justifyContent: "center",
  alignItems: "center",
 },
 bottomSheetContent: {
  padding: 16,
  backgroundColor: "#fff",
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20,
 },
 containerIcon: {
  alignItems: "flex-end",
 },
 textSuccess: {
  fontSize: 18,
  color: "#15F5BA",
  marginBottom: 10,
  fontWeight: "bold",
 },
 btnSubmit: {
  width: "90%",
  left: 0,
  right: 0,
  bottom: 20,
  padding: 15,
  borderRadius: 12,
  position: "absolute",
  backgroundColor: "#124076",
  marginHorizontal: "5%",
 },
 textBtn: {
  color: "white",
  fontSize: 16,
  textAlign: "center",
  fontWeight: "500",
 },
 border: {
  borderWidth: 0.5,
  padding: 20,
  borderRadius: 20,
  margin: 10,
 },
});

export default appStyles;

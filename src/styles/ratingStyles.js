import { StyleSheet } from "react-native";

const ratingStyles = StyleSheet.create({
 fontBold: {
  fontWeight: "bold",
  color: "#333A73",
  fontSize: 18,
  marginTop: 20,
  marginBottom: 20,
 },
 container: {
  alignItems: "center",
  justifyContent: "center",
 },
});

export default ratingStyles;

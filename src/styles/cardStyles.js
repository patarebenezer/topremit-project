import { StyleSheet } from "react-native";

const cardStyles = StyleSheet.create({
 container: {
  alignItems: "center",
  flexDirection: "row",
  justifyContent: "space-between",
 },
 containerCard: {
  alignItems: "center",
  flexDirection: "row",
 },
 textCard: {
  fontWeight: "500",
  fontSize: 16,
  color: "#45474B",
 },
 textMoney: {
  color: "#A9A9A9",
 },
 textSuccess: {
  fontSize: 16,
  color: "#15F5BA",
  marginBottom: 5,
  fontWeight: "bold",
 },
 textMoneys: {
  color: "#424769",
  fontWeight: "700",
  fontSize: 17,
 },
 textCardDate: {
  fontWeight: "500",
  marginRight: 30,
  color: "#45474B",
 },
 textCardDates: {
  fontWeight: "400",
  color: "#9BB8CD",
 },
 containerName: {
  marginLeft: -20,
 },
 containerCards: {
  flexDirection: "row",
  justifyContent: "space-between",
  width: "100%",
  alignItems: "center",
  marginBottom: 10,
 },
 cardView: {
  flexDirection: "row",
  alignItems: "center",
  gap: 10,
 },
 imageView: {
  width: 37,
  height: 25,
 },
});

export default cardStyles;
